const DEF_LOGIN = 'admin';
const DEF_PASS = 'superadmin';
const TOKEN = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c'

module.exports = function (req, res) {
  const body = req.body

  if (body.login === DEF_LOGIN && body.password === DEF_PASS) {
    res.jsonp({success: true, user_id: 1, token: TOKEN})
  }

  res.jsonp({success: false, message: 'Неверный логин или пароль'})
}
